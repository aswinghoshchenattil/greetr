<?php

namespace Aswin\Greetr;

class Greetr
{
    public function greet(String $sName)
    {
        // Unwanted comment test for sonarqube
        return 'Hi ' . $sName . '! Nice to meet you.';
    }
}
